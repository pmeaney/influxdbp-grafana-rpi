var express = require("express");
var router = express.Router();
const envConfig = require("../env-config.json");

const { InfluxDB } = require("@influxdata/influxdb-client");

// You can generate a Token from the "Tokens Tab" in the UI
const token = envConfig.INFLUX_TOKEN;
const org = envConfig.INFLUX_ORG;
const bucket = envConfig.INFLUX_BUCKET;
const client = new InfluxDB({ url: envConfig.INFLUX_CLIENT_URL, token: token });

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

router.get("/api/sensor_data", function (req, res, next) {
  console.log("[GET] Query params: ", req.query);

  /**
   * temperature: req.query.field_t
   * humidity: req.query.field_h
   */
  const { Point } = require("@influxdata/influxdb-client");
  const writeApi = client.getWriteApi(org, bucket);
  writeApi.useDefaultTags({ host: "host1" });

  const sensor_read__temperature = req.query.field_t
  const sensor_read__humidity = req.query.field_h

    const point1 = new Point("dht11_tempHumditiySensor")
    .tag("example", "write.ts")
    .floatField("temperature", sensor_read__temperature)
    .floatField("humidity", sensor_read__humidity);
    // to add another field: (e.g. could add stringField -- https://influxdata.github.io/influxdb-client-js/influxdb-client.point.stringfield.html)
    // .floatField("test", 999.99);
    // .floatField("value", 20 + Math.round(100 * Math.random()) / 10);
    
    // to add another field: (e.g. could add stringField -- https://influxdata.github.io/influxdb-client-js/influxdb-client.point.stringfield.html)
    // .floatField("test", 999.99);
    // .floatField("value", 20 + Math.round(100 * Math.random()) / 10);
    
    // const point2 = new Point("humidity")
    // .tag("example", "write.hs")
    // .floatField("humidity", sensor_read__humidity);
    
    // write points
    writeApi.writePoint(point1);
    console.log(` ${point1}`);
    // writeApi.writePoint(point2);
    // console.log(` ${point2}`);
  writeApi
    .close()
    .then(() => {
      console.log('NodeJS Timestamp: ' + new Date());
      console.log("FINISHED, wrote data1: ", point1);
      console.log("FINISHED, wrote data2: ", point2);
    })
    .catch((e) => {
      console.error(e);
      console.log("\\nFinished ERROR");
    });

  res.render("index", { title: "Thanks for the sensor data!", sensorData: { sensor_read__temperature, sensor_read__humidity } });

});
router.post("/post_data", function (req, res, next) {
  console.log("[POST] data has been posted. req.body: ", req.body);
  res.render("index", { title: "Thanks for data" });
});

// #### ################ #############################
// #### Example Route(s) #############################
// #### ################ #############################
// ________________ Routing examples from influxdb container nodejs start page ______________
router.get("/write_data", function (req, res, next) {
  const { Point } = require("@influxdata/influxdb-client");
  const writeApi = client.getWriteApi(org, bucket);
  writeApi.useDefaultTags({ host: "host1" });

  // const point = new Point("mem")
  //   .floatField("thing1", 20)
  //   .floatField("thing2", 35);

  const point1 = new Point("sensor1_reading")
    .tag("example", "write.ts")
    .floatField("value", 125);
    // .floatField("value", 20 + Math.round(100 * Math.random()) / 10);
    
    const point2 = new Point("humidity")
    .tag("example", "write.ts")
    .floatField("value", 61);
    
    // write points
    writeApi.writePoint(point1);
    console.log(` ${point1}`);
    writeApi.writePoint(point2);
    console.log(` ${point2}`);
  writeApi
    .close()
    .then(() => {
      console.log("FINISHED, wrote data1: ", point1);
      console.log("FINISHED, wrote data2: ", point2);
    })
    .catch((e) => {
      console.error(e);
      console.log("\\nFinished ERROR");
    });
});

router.get("/query", function (req, res, next) {
  const queryApi = client.getQueryApi(org);

  const query = `from(bucket: "${bucket}") |> range(start: -1h)`;
  queryApi.queryRows(query, {
    next(row, tableMeta) {
      const o = tableMeta.toObject(row);
      console.log(`${o._time} ${o._measurement} -- ${o._field}=${o._value}`);
      console.log("o object:", o);
    },
    error(error) {
      console.error(error);
      console.log("\\nFinished ERROR");
    },
    complete() {
      console.log("\\nFinished SUCCESS");
    },
  });
});

module.exports = router;
