

My ESP8266 is accessible via: /dev/cu.usbserial-013B5B92
To check your devices, run `ls /dev/cu.*`

https://blog.bandinelli.net/index.php?post/2016/11/08/ampy-from-Adafruit-to-connect-to-ESP8266

esptool.py --port /dev/cu.usbserial-013B5B92 --baud 460800 erase_flash

after downloading [the micropython firmware for esp8266 file](http://micropython.org/download/esp8266/), run this:
esptool.py --port /dev/cu.usbserial-013B5B92 --baud 460800 write_flash --flash_size=8m 0 esp8266-20210202-v1.14.bin


sudo env "PATH=$PATH" esptool.py --port /dev/cu.usbserial-013B5B92 --baud 921600 write_flash 0x00000 ../../ESP8266_NONOS_SDK/bin/eagle.flash.bin 0x10000 ../../ESP8266_NONOS_SDK/bin/eagle.irom0text.bin 0x3FB000 ../../ESP8266_NONOS_SDK/bin/blank.bin 0x3FC000 ../../ESP8266_NONOS_SDK/bin/esp_init_data_default_v08.bin 0x3FE000 ../../ESP8266_NONOS_SDK/bin/blank.bin


ESP 8266 Feather Huzzah breakout board

- [To reset esp8266 board](https://learn.adafruit.com/micropython-basics-how-to-load-micropython-on-a-board/esp8266): "For the HUZZAH ESP8266 breakout buttons for GPIO0 and RESET are built in to the board.  Hold GPIO0 down, then press and release RESET (while still holding GPIO0), and finally release GPIO0." 