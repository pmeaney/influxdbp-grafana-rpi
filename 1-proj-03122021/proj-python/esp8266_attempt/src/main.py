
import wifitools
import lib_dht11
import time

wifitools.connectToWifi()
// sleep before the start
time.sleep(5)

for i in range(8*60):
    data = lib_dht11.readTempAndHumidity()
    wifitools.send_post_data(data, 'https://testsite010.ml/esp8266_data')
    time.sleep(60)