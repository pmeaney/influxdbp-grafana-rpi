# ## original##
# # This file is executed on every boot (including wake-boot from deepsleep)
# --> (uncomment the following lines to see original):
#import esp
#esp.osdebug(None)
import gc
#import webrepl
#webrepl.start()
gc.collect()

####### / original ########

## modified
## re: https://pythonforundergradengineers.com/upload-py-files-to-esp8266-running-micropython.html

# This file is executed on every boot (including wake-boot from deepsleep)
# import esp
# esp.osdebug(None)
# import uos, machine
# import gc
# #import webrepl
# #webrepl.start()
# gc.collect()