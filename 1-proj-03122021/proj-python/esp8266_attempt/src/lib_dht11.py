def readTempAndHumidity():
  import dht
  import machine
  d = dht.DHT11(machine.Pin(2))
  d.measure()
  _temp = d.temperature()
  _humidity = d.humidity()
  return { 'temperature': _temp, 'humidity': _humidity }
