def makeReading():
    import Adafruit_DHT
    sensor = Adafruit_DHT.DHT11
    pin = 17
    humi, temp = Adafruit_DHT.read_retry(sensor, pin)
    tempConvertedToF = ((temp * 9/5) + 32)
    print('C temp: ' + str(temp))
    print('F temp: ' + str(tempConvertedToF))
    _humi = str(humi)
    _temp = str(tempConvertedToF)
    return { 'temperature': _temp, 'humidity': _humi }
   
def send_data_to_server(dataToSend):
    import requests
    temp_url_param = '?field_t=' + dataToSend['temperature']
    humi_url_param = '&field_h=' + dataToSend['humidity']
    url_params_as_data = temp_url_param + humi_url_param
    urlWithParams = 'https://www.testsite010.ml/api/sensor_data' + url_params_as_data
    makeRequest = requests.get(urlWithParams)
    print('sent data: ' + temp_url_param + ' ' + humi_url_param)
   

sensorDataToSend = makeReading()
print(sensorDataToSend)
send_data_to_server(sensorDataToSend)