
## See below for setup instructions

![grafana-dashboard](https://gitlab.com/pmeaney/influxdbp-grafana-rpi/-/raw/master/img/grafana-dashboard.png)



#### App Ports used:
- 3001
  - Express project.  Can change port in express-generated config file: proj-express/bin/www
- 3000
  - Grafana via Docker
- 8086
  - InfluxDB via Docker

This was hosted on a DigitalOcean linux server which has been previously configured with Nginx.  DigitalOcean has great articles on getting setting up on their servers and securing an Nginx server, so check out their blogs on those topics for more info.

##### Outline:
- Create & secure ubuntu server
- Set up nginx
- Set up an expressJS project
- For setting up Docker Containers:
  - See README section below "System > CLI Commands" for info on using sock5 port forwarding to easily access Docker containers from browser.
  - Create InfluxDB docker container.  Save the new account's  info.  Go to 'quick start', select nodejs, save token.
  - Create Grafana docker container.  Add data source of InfluxDB Docker container's IP adress, port 8086
- Adjust nginx configuration & Grafana configuration to proxy /grafana/ to the grafana container
- Set up rasberry pi to send sensor data to express server.

### Documentation
- Influx: 
  - [NodeJS Client Library](https://docs.influxdata.com/influxdb/v2.0/tools/client-libraries/js/)
  - [Nodejs client lib -- details on Point object methods](https://influxdata.github.io/influxdb-client-js/influxdb-client.point.html)
  - [Flux Query Language Docs](https://docs.influxdata.com/influxdb/cloud/query-data/flux/)

### System 
#### CLI Commands

- Open a sock5 port forwarding on browser:
  - References:
    - https://superuser.com/questions/408031/differences-between-ssh-l-to-d
    - https://dltj.org/article/ssh-as-socks-proxy/
    - https://stackoverflow.com/questions/35616095/explanation-of-ssh-command-with-option-d
```shell

# open sock5 proxy to server, set for host's at 5555 (arbitrarily set to 5555-- feel free to change it to whatever local host computer port you want to connect to the server)
ssh -D 5555 yourUser@yourServersIPaddress

# open chrome to the localhost port (5555) set above
"/Applications/Google Chrome.app/Contents/MacOS/Google Chrome" \ --user-data-dir="$HOME/proxy-profile" \ --proxy-server="socks5://localhost:5555"

# open the domain's IP at the particular port where the docker container is running.  For InfluxDB, it's 8086:
http://yourServersIPaddress:8086/signin
```

#### Docker stuff

- Grafana
  - https://grafana.com/docs/grafana/latest/administration/configure-docker/
  - Creating persistennt storage
    - `docker run -d -p 3000:3000 --name=grafana -v grafana-storage:/var/lib/grafana grafana/grafana`
  - Basic setup -- just for testing connection between sock5 and docker port
  - `docker run -d -p 3000:3000 grafana/grafana`

- InfluxDB
  - https://docs.influxdata.com/influxdb/v2.0/get-started/?t=Docker
  - Be sure to inspect which IP your InfluxDB docker container is running on.  This way you can connect to it via Grafana.
  - Inspect your container's IPs with:
    - `docker inspect -f '{{.Name}} - {{.NetworkSettings.IPAddress }}' $(docker ps -aq)`
    - My docker container IP & port, which I input into Grafana when adding the data source: http://172.17.0.2:8086

##### Grafana
- Initial login info is user: admin   pw: admin

- Configuration
  - https://grafana.com/docs/grafana/latest/administration/configuration/

- Settings > Data sources > InfluxDB (or Create) data source
  - Query language: Set to Flux (assuming you're using InfluxDB OSS Docker image which is InfluxDB 2.0)
  - Add in your:
    - Basic Auth Details 
    - InfluxDB Details

