# Query1
from(bucket: "bucket1")
  |> range(start: v.timeRangeStart, stop:v.timeRangeStop)
  |> filter(fn: (r) =>
    r._measurement == "temperature" and
    r._field == "value"
  )

# Query2
from(bucket: "bucket1")
  |> range(start: v.timeRangeStart, stop:v.timeRangeStop)
  |> filter(fn: (r) =>
    r._measurement == "humidity" and
    r._field == "value"
  )